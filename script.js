let firstNum = +prompt('Enter the first number....');
let secondNum = +prompt('Enter the second number....');
let arithmOperator = prompt("Enter arithmetic operators (' + ', ' - ', ' * ', ' / ')");

while (isNaN(firstNum) || isNaN(secondNum)) {
    alert('The values ​​entered are not numbers, please ENTER A NUMBER....');
    firstNum = +prompt('Enter the first number....', firstNum);
    secondNum = +prompt('Enter the second number....', secondNum);
}

while (arithmOperator !== '+' && arithmOperator !== '-' && arithmOperator !== '*' && arithmOperator !== '/') {
    alert('ERROR, please enter maths operator');
    arithmOperator = prompt("Enter arithmetic operators (' + ', ' - ', ' * ', ' / ')");
}

let result;
function getMathResult(firstNum, secondNum, arithmOperator) {
    switch (arithmOperator) {
        case '+':
            result = firstNum + secondNum;
            break;
        case '-':
            result = firstNum - secondNum;
            break;
        case '*':
            result = firstNum * secondNum;
            break;
        case '/':
            result = firstNum / secondNum;
            break;
        default:
            result = 'Error...please enter maths operator'
    }
    return result;
}

result = getMathResult(firstNum, secondNum, arithmOperator)
console.log(result);

//another method of executing the function
setTimeout(() => {
    let firstNum = +prompt('Enter the first number with timeout....');
    let secondNum = +prompt('Enter the second number with timeout....');
    let arithmOperator = prompt("Enter arithmetic operators with timeout (' + ', ' - ', ' * ', ' / ')");

    while (isNaN(firstNum) || isNaN(secondNum)) {
        alert('The values ​​entered are not numbers, please ENTER A NUMBER....');
        firstNum = +prompt('Enter the first number....', firstNum);
        secondNum = +prompt('Enter the second number....', secondNum);
    }

    while (arithmOperator !== '+' && arithmOperator !== '-' && arithmOperator !== '*' && arithmOperator !== '/') {
        alert('ERROR, please enter maths operator');
        arithmOperator = prompt("Enter arithmetic operators (' + ', ' - ', ' * ', ' / ')");
    }
    let result;
    function getResult(firstNum, secondNum, arithmOperator) {
        if (arithmOperator === '+') {
            result = firstNum + secondNum;
        }
        else if (arithmOperator === '-') {
            result = firstNum - secondNum;
        }
        else if (arithmOperator === '*') {
            result = firstNum * secondNum;
        }
        else if (arithmOperator === '-') {
            result = firstNum - secondNum;
        }
        else {
            result = 'ERROR, please enter maths operator...'
        }
        return result;

    }

    getResult(firstNum, secondNum, arithmOperator)
    console.log(result)
}, 2000)







